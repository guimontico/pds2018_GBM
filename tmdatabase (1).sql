-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29-Nov-2018 às 22:26
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tmdatabase`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `colaborador`
--

CREATE TABLE `colaborador` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `NumCracha` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `colaborador`
--

INSERT INTO `colaborador` (`id`, `username`, `NumCracha`, `email`, `password`) VALUES
(0, 'guilherme', '89924002', 'guimontico@outlook.com', '123'),
(4, 'caio', '123', 'caio@gmail.com', '89924003'),
(5, 'caio', '123', 'caio@gmail.com', '89924003'),
(6, 'gui', '', 'guimontico2@outlook.com', '202cb962ac59075b964b07152'),
(7, 'rafa', '', 'rafa@gmail.com', '123');

-- --------------------------------------------------------

--
-- Estrutura da tabela `colaboradorcurso`
--

CREATE TABLE `colaboradorcurso` (
  `IDColaboradorCurso` int(11) NOT NULL,
  `IDCurso` int(11) NOT NULL,
  `IDColaborador` int(11) NOT NULL,
  `DataCadastro` int(11) NOT NULL,
  `Motivo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `colaboradorcurso`
--

INSERT INTO `colaboradorcurso` (`IDColaboradorCurso`, `IDCurso`, `IDColaborador`, `DataCadastro`, `Motivo`) VALUES
(1, 1, 7, 2147483647, ' trabalho no setor de ti mano');

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE `curso` (
  `IDCurso` int(11) NOT NULL,
  `IDSetor` int(11) NOT NULL,
  `NomeCurso` varchar(100) NOT NULL,
  `Duracao` time NOT NULL,
  `NumAula` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`IDCurso`, `IDSetor`, `NomeCurso`, `Duracao`, `NumAula`) VALUES
(1, 1, 'Tecnicas de seguranÃ§a no TI', '00:50:00', 4),
(2, 2, 'empacotar', '00:50:00', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `exerc`
--

CREATE TABLE `exerc` (
  `IDExer` int(11) NOT NULL,
  `IDCurso` int(11) NOT NULL,
  `DescExer` varchar(500) NOT NULL,
  `OpcaoCorreta` varchar(100) NOT NULL,
  `Arquivo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `exerc`
--

INSERT INTO `exerc` (`IDExer`, `IDCurso`, `DescExer`, `OpcaoCorreta`, `Arquivo`) VALUES
(12, 1, '2 Ã© um numero natural?', 'sim', 'teste.jpg'),
(13, 1, '5 Ã© um numero natural?', 'sim', 'ototeste.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `setor`
--

CREATE TABLE `setor` (
  `IDSetor` int(11) NOT NULL,
  `NomeSetor` varchar(50) NOT NULL,
  `Gestor` varchar(100) NOT NULL,
  `CentroCusto` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `setor`
--

INSERT INTO `setor` (`IDSetor`, `NomeSetor`, `Gestor`, `CentroCusto`) VALUES
(1, 'TI', 'JosÃ©', '1050'),
(2, 'Logistica', 'Amauri', '1021');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `colaborador`
--
ALTER TABLE `colaborador`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colaboradorcurso`
--
ALTER TABLE `colaboradorcurso`
  ADD PRIMARY KEY (`IDColaboradorCurso`),
  ADD KEY `IDCurso` (`IDCurso`),
  ADD KEY `IDColaborador` (`IDColaborador`);

--
-- Indexes for table `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`IDCurso`),
  ADD KEY `IDSetor` (`IDSetor`);

--
-- Indexes for table `exerc`
--
ALTER TABLE `exerc`
  ADD PRIMARY KEY (`IDExer`),
  ADD KEY `IDCurso` (`IDCurso`);

--
-- Indexes for table `setor`
--
ALTER TABLE `setor`
  ADD PRIMARY KEY (`IDSetor`),
  ADD KEY `NomeSetor` (`NomeSetor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `colaborador`
--
ALTER TABLE `colaborador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `colaboradorcurso`
--
ALTER TABLE `colaboradorcurso`
  MODIFY `IDColaboradorCurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `curso`
--
ALTER TABLE `curso`
  MODIFY `IDCurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exerc`
--
ALTER TABLE `exerc`
  MODIFY `IDExer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `setor`
--
ALTER TABLE `setor`
  MODIFY `IDSetor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `colaboradorcurso`
--
ALTER TABLE `colaboradorcurso`
  ADD CONSTRAINT `colaboradorcurso_ibfk_2` FOREIGN KEY (`IDCurso`) REFERENCES `curso` (`IDCurso`);

--
-- Limitadores para a tabela `curso`
--
ALTER TABLE `curso`
  ADD CONSTRAINT `curso_ibfk_1` FOREIGN KEY (`IDSetor`) REFERENCES `setor` (`IDSetor`);

--
-- Limitadores para a tabela `exerc`
--
ALTER TABLE `exerc`
  ADD CONSTRAINT `exerc_ibfk_1` FOREIGN KEY (`IDCurso`) REFERENCES `curso` (`IDCurso`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
