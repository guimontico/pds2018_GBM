<?php
  session_start(); 

  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "Você deve logar antes";
  	header('location: login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }

    $hostname = '127.0.0.1';
    $dbname   = 'tmdatabase';
    $username = 'root'; 
    $password = '';
    $con = mysqli_connect($hostname, $username, $password) or DIE('Connection to host isailed, perhaps the service is down!');
    mysqli_select_db($con,$dbname) or DIE('Database name is not available!');


  
    $gettreino = "SELECT NomeCurso FROM curso";
    $query = mysqli_query($con, $gettreino);
    while ( $results[] = mysqli_fetch_object ( $query ) );
    array_pop ( $results );
    

    $querys = mysqli_query($con, "SELECT c.NomeCurso, cc.Motivo FROM curso as c JOIN colaboradorcurso as cc ON (c.IDCurso = cc.IDCurso) order by c.NomeCurso");
    

if (isset($_POST['btn_insert'])) {
    
    $meutreinamento = $_POST['meutreinamento'];
    $motivo = $_POST['motivo'];
    $user = $_SESSION['username'];
    
    
    $stmte = mysqli_query($con, "INSERT INTO colaboradorcurso (IDColaborador, IDCurso, Motivo, DataCadastro ) VALUES ((SELECT id FROM colaborador WHERE username = '" . $user . "'),(SELECT IDCurso FROM curso WHERE NomeCurso = '" . $meutreinamento . "'),'" . $motivo . "',NOW())");
    
    ?>
    <script>
        alert('Área Cadastrada ...');
//        window.location.href = 'contas_pagar.php';
    </script>

    <?php
}

if (isset($_POST['pesquisar'])) {
    
    $ptreinamento = $_POST['pesquisatreinamento'];  
    $otaq = mysqli_query($con, "SELECT IDCurso FROM curso WHERE NomeCurso = '" . $ptreinamento . "'");
    $row = mysqli_fetch_array($otaq); 
        
    ?>
    <script>
       window.location.href = 'treinamento.php?edit_id=<?php echo $row['IDCurso']; ?>';
    </script>
    <?php
}

?>

<!DOCTYPE HTML>
<!--
	Minimaxing by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Two Column 2 - Minimaxing by HTML5 UP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="CSS/main.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<header id="header">
									<h1><a href="home.php" id="logo">Treina Mais!</a></h1>
									<nav id="nav">
										<a href="home.php" class="current-page-item">Home</a>
										<a href="meustreinamentos.php">Meus Treinamentos</a>
                                        <a href="noticias.html">Noticias</a>
                                        <div class="dropdown">
                                          <a class="dropbtn">Cadastrar</a>
                                          <div class="dropdown-content">
                                            <a href="cadastro_treinamento.php">treinamentos</a>
                                            <a href="cadastro_aulas.php">aulas</a>
                                            <a href="cadastro_areas.php">areas</a>
                                          </div>
                                        
                                        </div>
									</nav>
								</header>
							</div>
						</div>
					</div>
				</div>

			<!-- Main -->
				<div id="main2">
                    
				    <div class="container">
                        
						<div class="row main-row">
							<div class="col-6">
                                <h2>Cadastrar Treinamentos</h2>
								<form method="post">
                                    <label for="ntrein">Treinamento</label>
                                        <select name="meutreinamento">
                                            <option>selecione a área</option>
                                             <?php foreach ( $results as $option ) : ?>
                                                  <option value="<?php echo $option->NomeCurso; ?>"><?php echo $option->NomeCurso; ?></option>
                                             <?php endforeach; ?>
                                        </select>
                                        <label for="duracao">Motivo</label>
                                        <input type="text" id="motivo" name="motivo" placeholder="Descreva porque está fazendo o curso.">
                                    <input name="btn_insert" type="submit" value="Submit">
                                </form>
							</div>
                            <div class="col-6 ">
				                <h2>Pesquisar treinamentos</h2>
								<section>
									<div class="container">
                                      <form method="post" >
                                      
                                        <label for="treinamento">Treinamento</label>
                                        <select name="pesquisatreinamento">
                                            <option>selecione o treinamento</option>
                                             <?php foreach ( $results as $option ) : ?>
                                                  <option onselect="" value="<?php echo $option->NomeCurso; ?>"><?php echo $option->NomeCurso; ?></option>
                                             <?php endforeach; ?>
                                        </select>
                                        <input type="submit" name="pesquisar" value="Pesquisar">
                                      </form>
                                    </div>
								</section>
							</div>
						</div>
					</div>
				</div>


			<!-- Footer -->
				<div id="footer-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-8 col-12-medium">

								<section>
									<h2>How about a truckload of links?</h2>
									<div>
										<div class="row">
											<div class="col-3 col-6-medium col-12-small">
												<ul class="link-list">
													<li><a href="#">Sed neque nisi consequat</a></li>
													<li><a href="#">Dapibus sed mattis blandit</a></li>
													<li><a href="#">Quis accumsan lorem</a></li>
													<li><a href="#">Suspendisse varius ipsum</a></li>
													<li><a href="#">Eget et amet consequat</a></li>
												</ul>
											</div>
											<div class="col-3 col-6-medium col-12-small">
												<ul class="link-list">
													<li><a href="#">Quis accumsan lorem</a></li>
													<li><a href="#">Sed neque nisi consequat</a></li>
													<li><a href="#">Eget et amet consequat</a></li>
													<li><a href="#">Dapibus sed mattis blandit</a></li>
													<li><a href="#">Vitae magna sed dolore</a></li>
												</ul>
											</div>
											<div class="col-3 col-6-medium col-12-small">
												<ul class="link-list">
													<li><a href="#">Sed neque nisi consequat</a></li>
													<li><a href="#">Dapibus sed mattis blandit</a></li>
													<li><a href="#">Quis accumsan lorem</a></li>
													<li><a href="#">Suspendisse varius ipsum</a></li>
													<li><a href="#">Eget et amet consequat</a></li>
												</ul>
											</div>
											<div class="col-3 col-6-medium col-12-small">
												<ul class="link-list">
													<li><a href="#">Quis accumsan lorem</a></li>
													<li><a href="#">Sed neque nisi consequat</a></li>
													<li><a href="#">Eget et amet consequat</a></li>
													<li><a href="#">Dapibus sed mattis blandit</a></li>
													<li><a href="#">Vitae magna sed dolore</a></li>
												</ul>
											</div>
										</div>
									</div>
								</section>

							</div>
							<div class="col-4 col-12-medium">

								<section>
									<h2>Something of interest</h2>
									<p>Duis neque nisi, dapibus sed mattis quis, rutrum accumsan sed.
									Suspendisse eu varius nibh. Suspendisse vitae magna eget odio amet
									mollis justo facilisis quis. Sed sagittis mauris amet tellus gravida
									lorem ipsum dolor sit blandit.</p>
									<footer class="controls">
										<a href="#" class="button">Oh, please continue ....</a>
									</footer>
								</section>

							</div>
						</div>
						<div class="row">
							<div class="col-12">

								<div id="copyright">
									&copy; Untitled. All rights reserved. | Design: <a href="http://html5up.net">HTML5 UP</a>
								</div>

							</div>
						</div>
					</div>
				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>