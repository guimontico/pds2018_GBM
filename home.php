<?php 
  session_start(); 

  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }
?>

<!DOCTYPE HTML>
<!--
	Minimaxing by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Treina Mais!</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="CSS/main.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<header id="header">
									<h1><a href="home.php" id="logo">Treina Mais!</a></h1>
									<nav id="nav">
										<a href="home.php" class="current-page-item">Home</a>
										<a href="meustreinamentos.php">Meus Treinamentos</a>
                                        <a href="noticias.html">Noticias</a>
                                        <div class="dropdown">
                                          <a class="dropbtn">Cadastrar</a>
                                          <div class="dropdown-content">
                                            <a href="cadastro_treinamento.php">treinamentos</a>
                                            <a href="cadastro_aulas.php">aulas</a>
                                            <a href="cadastro_areas.php">areas</a>
                                          </div>
                                        
                                        </div>
									</nav>
								</header>
							</div>
						</div>
					</div>
				</div>

			<!-- Banner -->
				<div id="banner-wrapper">
					<div class="container">
						<div id="banner">
							<h2>Treina Mais! - Home Page</h2>
							<span>Central de treinamentos e cursos empresariais</span>
						</div>
					</div>
				</div>

			<!-- Main -->
				<div id="main">
					<div class="container">
						<div class="row main-row">
							<div class="col-4 col-12-medium">
                                <h2>Pesquisar treinamentos</h2>
								<section>
									<div class="container">
                                      <form action="/action_page.php">
                                        <label for="fname">Nome do Curso</label>
                                            <input type="text" id="fname" name="firstname" placeholder="">
                                        <label for="lname">Centro de Custo</label>
                                            <input type="text" id="lname" name="lastname" placeholder="">
                                        <label for="area">Área</label>
                                        <select name="area">
                                            <option>selecione a área</option>
                                             <?php foreach ( $results as $option ) : ?>
                                                  <option value="<?php echo $option->NomeSetor; ?>"><?php echo $option->NomeSetor; ?></option>
                                             <?php endforeach; ?>
                                        </select>
                                        <input type="submit" value="Pesquisar">
                                        <input type="reset" value="Cancelar">
                                      </form>
                                    </div>
								</section>
							</div>
							<div class="col-4 col-6-medium col-12-small">
								<section>
									<h2>Novos Treinamentos</h2>
									<ul class="small-image-list">
										<li>
											<a href="#"><img src="images/pic2.jpg" alt="" class="left" /></a>
											<a href="#"><h4>TO-04(Ordem de compra)</h4></a>
											<p>Abertura de ordem de compra, como executar e como aprovar.</p>
										</li>
										<li>
											<a href="#"><img src="images/pic1.jpg" alt="" class="left" /></a>
											<a href="#"><h4>TA-02(Solicitação de Melhoria)</h4></a>
											<p>Como realizar a abertura de solicitação de melhoria e para onde enviar.</p>
										</li>
                                        <li>
											<a href="#"><img src="images/pic7.jpg" alt="" class="left" /></a>
											<a href="#"><h4>TH-06(Boas Praticas)</h4></a>
											<p>Como realizar seu trabalho com ética e boas práticas</p>
										</li>
									</ul>
								</section>

							</div>
							<div class="col-4 col-6-medium col-12-small">

								<section>
									<h2>Novidades e notícias</h2>
									<div>
										<div class="row">
											<div class="col-6 col-12-small">
												<ul class="link-list">
													<li><a href="#">Sed neque nisi consequat</a></li>
													<li><a href="#">Dapibus sed mattis blandit</a></li>
													<li><a href="#">Quis accumsan lorem</a></li>
													<li><a href="#">Suspendisse varius ipsum</a></li>
													<li><a href="#">Eget et amet consequat</a></li>
												</ul>
											</div>
										</div>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>

			<!-- Footer -->
				<div id="footer-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-8 col-12-medium">

								<section>
									<h2>How about a truckload of links?</h2>
									<div>
										<div class="row">
											<div class="col-3 col-6-medium col-12-small">
												<ul class="link-list">
													<li><a href="#">Sed neque nisi consequat</a></li>
													
												</ul>
											</div>
											<div class="col-3 col-6-medium col-12-small">
												<ul class="link-list">
													<li><a href="#">Quis accumsan lorem</a></li>
												
												</ul>
											</div>
											<div class="col-3 col-6-medium col-12-small">
												<ul class="link-list">
													<li><a href="#">Sed neque nisi consequat</a></li>
												
												</ul>
											</div>
											<div class="col-3 col-6-medium col-12-small">
												<ul class="link-list">
													<li><a href="#">Quis accumsan lorem</a></li>
													
												</ul>
											</div>
										</div>
									</div>
								</section>
							</div>
						</div>
						<div class="row">
							<div class="col-12">

								<div id="copyright">
									&copy; Untitled. All rights reserved. | Design: <a href="http://html5up.net">HTML5 UP</a>
								</div>

							</div>
						</div>
					</div>
				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>