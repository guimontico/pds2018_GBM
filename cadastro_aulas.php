<?php
    session_start();
    $hostname = '127.0.0.1';
    $dbname   = 'tmdatabase';
    $username = 'root'; 
    $password = '';
    $con = mysqli_connect($hostname, $username, $password) or DIE('Connection to host isailed, perhaps the service is down!');
    mysqli_select_db($con,$dbname) or DIE('Database name is not available!');

    $getcurso = "SELECT NomeCurso FROM curso";
     $query = mysqli_query($con, $getcurso);
     while ( $results[] = mysqli_fetch_object ( $query ) );
     array_pop ( $results );


if (isset($_POST['btn_insert'])) {


    $select = $_POST['selecttrein'];
    $exer = $_POST['exer'];
    $correta = $_POST['correta']; 

			 $arquivo = $_FILES['arquivo']['name'];
			
			//Pasta onde o arquivo vai ser salvo
			$_UP['pasta'] = 'foto/';
			
			//Tamanho máximo do arquivo em Bytes
			$_UP['tamanho'] = 1024*1024*100; //5mb
			
			//Array com a extensões permitidas
			$_UP['extensoes'] = array('png', 'jpg', 'jpeg', 'gif');
			
			//Renomeiar
			$_UP['renomeia'] = false;
			
			//Array com os tipos de erros de upload do PHP
			$_UP['erros'][0] = 'Não houve erro';
			$_UP['erros'][1] = 'O arquivo no upload é maior que o limite do PHP';
			$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especificado no HTML';
			$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
			$_UP['erros'][4] = 'Não foi feito o upload do arquivo';
			
			//Verifica se houve algum erro com o upload. Sem sim, exibe a mensagem do erro
			if($_FILES['arquivo']['error'] != 0){
				die("Não foi possivel fazer o upload, erro: <br />". $_UP['erros'][$_FILES['arquivo']['error']]);
				exit; //Para a execução do script
			}
			
			//O arquivo passou em todas as verificações, hora de tentar move-lo para a pasta foto
			else{
                $nome_final = $_FILES['arquivo']['name'];
				//Verificar se é possivel mover o arquivo para a pasta escolhida
				if(move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'].$nome_final)){
					//Upload efetuado com sucesso, exibe a mensagem
                     $stmt = mysqli_query($con, "INSERT INTO exerc(IDCurso, DescExer, OpcaoCorreta, Arquivo) VALUES ((SELECT IDCurso FROM curso WHERE NomeCurso = '" . $select . "'),'" . $exer . "','" . $correta . "', '" . $nome_final . "')");
				}
			}
			
			
		?>
    <script>
        alert('Exercicio Cadastrada ...');
//        window.location.href = 'contas_pagar.php';
    </script>

    <?php
}
?>

<!DOCTYPE HTML>
<!--
	Minimaxing by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Two Column 2 - Minimaxing by HTML5 UP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="CSS/main.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<header id="header">
									<h1><a href="home.php" id="logo">Treina Mais!</a></h1>
									<nav id="nav">
										<a href="home.php" class="current-page-item">Home</a>
										<a href="meustreinamentos.php">Meus Treinamentos</a>
                                        <a href="noticias.html">Noticias</a>
                                        <div class="dropdown">
                                          <a class="dropbtn">Cadastrar</a>
                                          <div class="dropdown-content">
                                            <a href="cadastro_treinamento.php">treinamentos</a>
                                            <a href="cadastro_aulas.html">aulas</a>
                                            <a href="cadastro_areas.php">areas</a>
                                          </div>
                                        </div>
									</nav>
								</header>
							</div>
						</div>
					</div>
				</div>

			<!-- Main -->
				<div id="main2">
					<div class="container">
				        <div class="col-4 ">
                            <h2>Cadastrar Treinamentos</h2>
                            <div class="cadastroarea">
                                <div class="col-10">
                                    <form method="post" enctype="multipart/form-data">
                                        <label for="ntrein">Selecione o treinamento</label>
                                            <select name="selecttrein">
                                            <option>selecione o treinamento</option>
                                             <?php foreach ( $results as $option ) : ?>
                                                  <option onselect="" value="<?php echo $option->NomeCurso; ?>"><?php echo $option->NomeCurso; ?></option>
                                             <?php endforeach; ?>
                                        </select>
                                        <label for="area">Upload do Material (PDF)</label><br><br>
                                        <input name="arquivo" type="file"><br><br>
                                            <label for="duracao">Exercício</label>
                                            <input type="text" id="exer" name="exer" placeholder="">
                                        <label for="numaula">Insira a resposta correta</label>
                                            <input type="text" id="correta" name="correta" placeholder="">
                                        <input name="btn_insert" type="submit" value="Submit">
                                    </form>
                                </div>
                            </div>
				        </div>
					</div>
				</div>

			<!-- Footer -->
				<div id="footer-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-8 col-12-medium">

							</div>
							<div class="col-4 col-12-medium">


							</div>
						</div>
						<div class="row">
							<div class="col-12">

								<div id="copyright">
									&copy; Untitled. All rights reserved. | Design: <a href="http://html5up.net">HTML5 UP</a>
								</div>

							</div>
						</div>
					</div>
				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>